const fs = require("fs");

const dirPath = "./data";
if ( !fs.existsSync(dirPath) ) {
	fs.mkdirSync(dirPath);
};

const dirFile = "./data/db.json";
if ( !fs.existsSync(dirFile) ) {
	fs.writeFileSync(dirFile, "[]", "utf8");
};

const loadData = id => {
	const contacts = JSON.parse(fs.readFileSync(dirFile, "utf8"));
	return contacts;
};

const getData = id => {
	const contacts = loadData();
	return contacts.find(contact => contact.id === id);
};

const saveData = value => {
	fs.writeFileSync(dirFile, JSON.stringify(value), "utf8");
}

const createData = value => {
	const data = loadData();
	let id = "1";
	if ( data.length > 0) {
		id = `${parseInt(data[data.length - 1].id) + 1}`;
	}
	const newData = { id, ...value };
	data.push(newData);
	saveData(data);
};

const cekDuplikatEmail = email => {
	const data = loadData();
	return data.find(contact => contact.email === email);
};

const cekDuplikatNomor = nomor => {
	const data = loadData();
	return data.find(contact => contact.nomor === nomor);
};

const deleteData = id => {
	const data = loadData();
	const newData = data.filter(contact => contact.id !== id);
	saveData(newData);
};

const cekUpdateEmail = (value, id) => {
	const data = loadData();
	const dataContact = getData(id);
	console.log(id);
	return data.filter(cntct => cntct.email !== dataContact.email).find(data => data.email === value);
}

const cekUpdateNomor = (value, id) => {
	const data = loadData();
	const dataContact = getData(id);
	return data.filter(cntct => cntct.nomor !== dataContact.nomor).find(data => data.nomor === value);
}

const updateData = (value, id) => {
	const data = loadData();
	const isiData = data.filter(contact => contact.id !== id);
	let idBaru;
	if ( isiData.length > 0) {
		idBaru = `${parseInt(isiData[isiData.length - 1].id) + 1}`;
	};
	const newContact = {id: idBaru, ...value};
	isiData.push(newContact);
	saveData(isiData);
};

module.exports = {
	loadData,
	getData,
	createData,
	cekDuplikatEmail,
	cekDuplikatNomor,
	deleteData,
	cekUpdateEmail,
	cekUpdateNomor,
	updateData,
}
