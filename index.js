const express = require("express");
const app = express();
const expressLayouts = require("express-ejs-layouts");
const flash = require("connect-flash");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const { body, validationResult, check } = require("express-validator");
const path = require("path");
const port = 3000;
const { loadData, getData, createData, cekDuplikatEmail, cekDuplikatNomor, deleteData, cekUpdateEmail, cekUpdateNomor, updateData } = require("./utils/app");

app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(path.join(__dirname + '/public')));
app.use(cookieParser("secret"));
app.use(session({
	secret: "secret",
	maxAge: 3600,
	resave: true,
	saveUninitialized: true
}));
app.use(express.urlencoded({extended: true}));
app.use(flash());

app.get("/", (req, res) => {
	res.render("index", {
		layout: "./layouts/base",
		title: "Home"
	});
});

app.get("/about", (req, res) => {
	res.render("about", {
		layout: "./layouts/base",
		title: "about",
	});
});

app.get("/contact", (req, res) => {
	const dataContact = loadData();
	res.render("contact", {
		layout: "./layouts/base",
		title: "Contact",
		dataContact,
		msg: req.flash("msg")
	});
});

app.post("/contact", [
	check("email", "Email tidak valid").isEmail(),
	body("email").custom(value => {
		const duplikatEmail = cekDuplikatEmail(value);
		if (duplikatEmail) {
			throw new Error("Email sudah digunakan");
		}
		return true;
	}),
	check("nomor", "Nomor tidak valid").isMobilePhone("id-ID"),
	body("nomor").custom(value => {
		const duplikatNomor = cekDuplikatNomor(value);
		if (duplikatNomor) {
			throw new Error("Nomor sudah digunakan");
		}
		return true
	})
], (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		res.render("create-data", {
			layout: "./layouts/base",
			title: "create data",
			errors: errors.array()
		});
	} else {
		createData(req.body);
		req.flash("msg", "Data berhasil ditambahkan");
		res.redirect("/contact");
	}
});

app.get("/contact/create/data", (req, res) => {
	res.render("create-data", {
		layout: "./layouts/base",
		title: "create data"
	});
});

app.get("/contact/delete/:id", (req, res) => {
	const id = req.params.id;
	const contact = getData(id);
	if (!contact) {
		res.status(404);
		res.send("<h1>404 not found</h1>");
	} else {
		deleteData(id);
		req.flash("msg", "Data berhasil dihapus");
		res.redirect("/contact");
	};
});

app.get("/contact/update/:id", (req, res) => {
	const id = req.params.id;
	const contact = getData(id);
	res.render("update-data", {
		layout: "./layouts/base",
		title: "Update data",
		contact
	});
});

app.post("/contact/update/", [
	check("email", "Email tidak valid").isEmail(),
	body("email").custom((value, {req}) => {
		const updateEmail = cekUpdateEmail(value, req.body.id);
		if(typeof updateEmail === "object") {
			throw new Error("Email sudah digunakan");
		};
		return true;
	}),
	check("nomor", "Nomor tidak valid").isMobilePhone("id-ID"),
	body("nomor").custom((value, {req}) => {
		const updateNomor = cekUpdateNomor(value, req.body.id);
		if(typeof updateNomor === "object") {
			throw new Error("Nomor sudah digunakan");
		}
		return true
	})
], (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		res.render("update-data", {
			layout: "./layouts/base",
			title: "update data",
			errors: errors.array(),
			contact: req.body
		});
	} else {
		updateData(req.body, req.body.id);
		req.flash("msg", "Data berhasil diupdate");
		res.redirect("/contact");
	}
});

app.get("/contact/:id", (req, res) => {
	const id = req.params.id;
	const contact = getData(id);
	res.render("detail", {
		layout: "./layouts/base",
		title: "detail",
		contact,
	});
});

app.use("/", (req, res) => {
	res.status(404);
	res.send("<h1>404 not found</h1>");
});

app.listen(port, () => { console.log("Server listen at http://localhost:3000")})
